/*******************************************************************************
 * Copyright (c) 2014, Sean McAllister
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * The views and conclusions contained in the software and documentation are those
 * of the authors and should not be interpreted as representing official policies, 
 * either expressed or implied, of the FreeBSD Project.
 *******************************************************************************/

#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

#include <limits>

#include <smoothsort.h>

// see "Engineering a Sort Function" by Bentley and McIlroy for a reference on these
// basically we want to generate a range of troublesome sort cases and evaluate the
// algorithm versus a reference implementation.
typedef enum { SAWTOOTH, RAND, STAGGER, PLATEAU, SHUFFLE                   } sig_type_t;
typedef enum { COPY, REVERSE_ALL, REVERSE_FST, REVERSE_SND, SORTED, DITHER } mod_type_t;

const char* signame(sig_type_t type) {
    switch(type) {
        case SAWTOOTH:    return "SAWTOOTH";
        case RAND:        return "RAND";
        case STAGGER:     return "STAGGER";
        case PLATEAU:     return "PLATEAU";
        case SHUFFLE:     return "SHUFFLE";
    }
    return NULL;
}

const char* modname(mod_type_t mod) {
    switch(mod) {
        case COPY:        return "COPY";
        case REVERSE_ALL: return "REVERSE_ALL";
        case REVERSE_FST: return "REVERSE_FST";
        case REVERSE_SND: return "REVERSE_SND";
        case SORTED:      return "SORTED";
        case DITHER:      return "DITHER";
    }
    return NULL;
}


// wrapper around < operator for comparison
template <typename T> int compare(const void* pa, const void *pb) {
    T *aa = (T*)pa, *bb = (T*)pb;

    if (*aa < *bb) return -1;
    if (*aa > *bb) return +1;
    return 0;
}


// generate a random array of the given size and type
template <typename T> T* randarray(size_t size, size_t m, sig_type_t type) {
    T* arr = (T*)malloc(size*sizeof(T));
    for (size_t ii=0, jj=0, kk=1; ii < size; ii++) {
        switch(type) {
            case SAWTOOTH: arr[ii] = ii     % m;                         break;
            case RAND:     arr[ii] = rand() % m;                         break;
            case STAGGER:  arr[ii] = (ii*m+ii) % size;                   break;
            case PLATEAU:  arr[ii] = (ii < m) ? ii : m;                  break;           
            case SHUFFLE:  arr[ii] = rand() % m ? (jj += 1) : (kk += 2); break;
        }
    }
    return arr;
}

// reverse an array in place
template <typename T> void reverse(T* array, size_t nmemb) {
    size_t beg=0;
    size_t end=nmemb-1;
    while (end > beg) {
        leo_swap(&array[beg], &array[end], sizeof(T));
        end--;
        beg++;
    }
}


// dither an array by adding ii % 5 to each element
template <typename T> void dither(T* array, size_t nmemb) {
    for (size_t ii=0; ii < nmemb; ii++) {
        array[ii] += ii % 5;
    }
}


#define NMEMB 100000
#define ITER  20

template <typename T>
bool benchmark_(
    void (*sort)(void*, size_t, size_t, cmp_func_t),
    size_t mm, 
    sig_type_t sigtype,
    mod_type_t modtype, 
    double *elapsed, 
    const char* sortname,
    const char* type
) {
    struct timeval beg, end;                                            
    *elapsed=0.0;;                                                

    bool all_passed = true;
    for (size_t ii=0; ii < ITER; ii++) {                                
        T *arr = randarray<T>(NMEMB, mm, sigtype);

        switch(modtype) {
            case COPY:                                                 break;
            case REVERSE_ALL: reverse(arr,         NMEMB);             break;
            case REVERSE_FST: reverse(arr,         NMEMB/2);           break;
            case REVERSE_SND: reverse(arr+NMEMB/2, NMEMB/2);           break;    
            case DITHER:       dither(arr,         NMEMB);             break;
            case SORTED:      sort(arr, NMEMB, sizeof(T), compare<T>); break;
        }
        
        fprintf(stderr, "\r%10s %8s %12s %7zd %6s iter: %4zd/%zd", sortname, signame(sigtype), modname(modtype), mm, type, ii+1, (size_t)ITER);
        
        gettimeofday(&beg, NULL);                                       
        sort(arr, NMEMB, sizeof(T), compare<T>);                  
        gettimeofday(&end, NULL);                                       
        
        *elapsed +=                                                      
            ((double)end.tv_sec  - beg.tv_sec) +                        
            ((double)end.tv_usec - beg.tv_usec)/1000000;                
        
        bool all=true;                                                  
        for (size_t jj=0; jj < NMEMB-1; jj++) {                         
            all &= (arr[jj] <= arr[jj+1]);                              
        }                                                               
        free(arr);
        
        if (!all) {                                                     
            fprintf(stderr, "- failed\n");                              
            break;                                                      
        }                                                               
        
        all_passed &= all;
    }
    
    return all_passed;
}
#define benchmark(type, mm, sigtype, modtype, sort, elapsed) benchmark_<type>(sort, mm, sigtype, modtype, elapsed, #sort, #type)



int main() {
    srand(time(NULL));

    bool all_passed = true;
    double ratio=0.0;
    double qelapsed, selapsed;
    size_t cnt=0;
    for (int type=0; type <= SHUFFLE; type++) {         // iterate over all waveform types
        for (int mod=0; mod <= DITHER; mod++) {         // and all modification to them
            for (size_t mm=1; mm < 2*NMEMB; mm *= 2) {  // vary the parameter for dithering, etc.

                if (all_passed) {
                    all_passed &= benchmark(float, mm, (sig_type_t)type, (mod_type_t)mod, qsort, &qelapsed);
                    if(all_passed) {
                        printf(" -- time: %e\n", qelapsed/ITER);
                    }
                }
                
                if (all_passed) {
                    ratio += selapsed/qelapsed;
                    all_passed &= benchmark(float, mm, (sig_type_t)type, (mod_type_t)mod, smoothsort, &selapsed);
                    if(all_passed) {
                        printf(" -- time: %e  %.3fx\n", selapsed/ITER, selapsed/qelapsed);
                    }
                }
                cnt++;
            }
            printf("\n\n");
        }
    }

    printf("average slowdown vs reference sort: %.3fx\n", ratio/cnt);
    
    return 0;
}
