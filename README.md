# Smoothsort #

### Description of algorithm ###
This repo contains an implementation of Dijkstra's smoothsort algorithm.  This algorithm is a modification 
of the Heapsort algorithm which has the special property that insertion of a new maximum element into the
heap is O(1) complexity.  This feature means that the algorithm can smoothly vary between O(n*lg n) and 
O(n) complexity depending on how sorted the input array is to begin with.

To achieve the O(1) complexity for maximum-insert property, smoothsort uses a data structure based on the Leonardo numbers called a Leonardo heap.  This heap is really composed of a forest of binary trees that have two defining properties.  First, each tree must have a size equal to one of the Leonardo numbers.  Second, the nodes of the tree must satisfy the heap property.  That is, no node can have a parent greater than itself, which ensures that the maximum node in each tree is at the top.  In addition to the tree properties, we also require that the root nodes of the trees be in ascending order.  This ensures that the maximum item in the heap is always in its proper place.

Leonardo numbers are defined recursively, in a manner similar to the Fibbonacci numbers.

    L(0) = 1
    L(1) = 1
    L(n) = L(n-2) + L(n-1) + 1

From this definition, the first 5 Leonardo numbers are then 1,1,3,5, and 9. 

The recursive definition of Leonardo numbers means that when we add an element to the heap, if the previous two trees are sized by consecutive Leonardo numbers L(n) and L(n-1), we can merge them into a new tree with  Leonardo number L(n+1).

The smoothsort algorithm then boils down to building a Leonardo heap in-place over the data, which requires keeping track of what Leonardo trees exist in the heap currently.  Presuming a 64-bit machine, there are only 92 Leonardo numbers < 2**64 and thus with those we can represent any array we might encounter on our architecture.  Thus an array of 92 8-bit integers representing the Leonardo number indices is all the overhead we require for the sort.

With that said, the algorithm is broken down into two stages: insertion and removal.

#### Insertion ####
In the insertion stage, we one by one insert the elements of the array to be sorted into the Leonardo heap.  This is done as follows.  For each element:

Look at the last two trees in the heap.  If they have consecutive Leonardo numbers L(n) and L(n-1), merge them into a tree with number L(n+1) with the new item as the root.  Otherwise, if there's no L(1) tree in the heap, add the new item as an L(1) tree.  Otherwise add it as an L(0) tree.

Fixup the heap.  Starting with the tree we just put the new item in, look at the previous tree in the list, if its root node is greater than the current trees root _and_ its two children, then swap the current root with the previous root.  Move to the previous tree in the heap and repeat until there's nothing more to be swapped or we run out of previous trees.

Once the node has been swapped to the right tree, then we have to restore its heap property. To do this, compare the root with its two children.  If either one is larger than the root, swap it with the larger of the two.  Then recursively process the subtree that the root was swapped into until we reach the bottom.

These two steps ensure that the max value of the heap is always in the rightmost position and each tree in the heap is heap-ordered.


#### Removal ####
Since the above steps ensure the max item is always in its correct sorted place, we can just repeatedly take the last root out of the heap, and rebalance everything.  This is done as follows.  While there's still elements in the heap:

If the last tree in the heap is L(1) or L(0), remove it from the list and there's nothing else to do.  Otherwise, split the tree into an L(n-1) and L(n-2) tree.  Fix up the left-tree and then the right-tree in the same fashion as with the insertion step.  


### API ###
The smoothsort.h file exposes a single function of interest:

    static inline void smoothsort(void *dptr, size_t nmemb, size_t size, int (*cmp)(const void* a, const void *b));

Which is defined to match the _qsort_ function provided in the C standard library.