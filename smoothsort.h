/*******************************************************************************
 * Copyright (c) 2014, Sean McAllister
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * The views and conclusions contained in the software and documentation are those
 * of the authors and should not be interpreted as representing official policies, 
 * either expressed or implied, of the FreeBSD Project.
 *******************************************************************************/

#ifndef SMOOTHSORT_
#define SMOOTHSORT_

#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <stdbool.h>

// leonardo numbers < 2**64.  Defined recursively as L(n) = L(n-1)+L(n-2)+1
#define NLEONARDO 92
static const uint64_t LEONARDO[] = {
                      1ull,                   1ull,                   3ull,                    5ull,
                      9ull,                  15ull,                  25ull,                   41ull,
                     67ull,                 109ull,                 177ull,                  287ull,
                    465ull,                 753ull,                1219ull,                 1973ull,
                   3193ull,                5167ull,                8361ull,                13529ull,
                  21891ull,               35421ull,               57313ull,                92735ull,
                 150049ull,              242785ull,              392835ull,               635621ull,
                1028457ull,             1664079ull,             2692537ull,              4356617ull,
                7049155ull,            11405773ull,            18454929ull,             29860703ull,
               48315633ull,            78176337ull,           126491971ull,            204668309ull,
              331160281ull,           535828591ull,           866988873ull,           1402817465ull,
             2269806339ull,          3672623805ull,          5942430145ull,           9615053951ull,
            15557484097ull,         25172538049ull,         40730022147ull,          65902560197ull,
           106632582345ull,        172535142543ull,        279167724889ull,         451702867433ull,
           730870592323ull,       1182573459757ull,       1913444052081ull,        3096017511839ull,
          5009461563921ull,       8105479075761ull,      13114940639683ull,       21220419715445ull,
         34335360355129ull,      55555780070575ull,      89891140425705ull,      145446920496281ull,
        235338060921987ull,     380784981418269ull,     616123042340257ull,      996908023758527ull,
       1613031066098785ull,    2609939089857313ull,    4222970155956099ull,     6832909245813413ull,
      11055879401769513ull,   17888788647582927ull,   28944668049352441ull,    46833456696935369ull,
      75778124746287811ull,  122611581443223181ull,  198389706189510993ull,   321001287632734175ull,
     519390993822245169ull,  840392281454979345ull, 1359783275277224515ull,  2200175556732203861ull,
    3559958832009428377ull, 5760134388741632239ull, 9320093220751060617ull, 15080227609492692857ull
};

// comparison function type
typedef int (*cmp_func_t)(const void*, const void*); 

typedef struct {
    uint8_t    lnum[NLEONARDO]; // leonardo tree numbers making up the heap
    size_t     ntree;           // number of leonardo trees in the heap

    void      *data;            // pointer to data 
    size_t     nmemb;           // number of points in data
    size_t     size;            // size of each point in bytes
    cmp_func_t cmp;             // comparison function
} leo_heap_t; 


// does a simple byte-by-byte swap.  Benchmarked using a word-based swap and it
// made very little difference.
static inline void leo_swap(void *aa, void *bb, size_t size) {
    char tmp;
    for (size_t ii=0; ii < size; ii++) {
                    tmp = ((char*)aa)[ii];
        ((char*)aa)[ii] = ((char*)bb)[ii];
        ((char*)bb)[ii] = tmp;
    }
}


// compare-and-swap function for data, return true if swapped
static inline bool leo_cmp_and_swap(void* aa, void* bb, size_t size, cmp_func_t cmp) {
    if (cmp(aa, bb) >= 1) {
        leo_swap(aa, bb, size);
        return true;
    }
    return false;
}


/*******************************************************************************
 * Restore the heap after insertion/deletion.  This swaps root nodes as
 * required to place root in its proper place and bubbles up final modified tree
 * to restore its heap property.
 *
 * @param heap  pointer to heap_t instance to restore
 * @param tree  index of tree to restore
 * @param index index to root of tree
 *******************************************************************************/
static inline void leo_heap_restore_(leo_heap_t *heap, size_t tree, size_t index) {
    // find tree where root node belongs
    size_t curtree = tree;
    char*  root    = (char*)heap->data + index*heap->size;
    while (curtree > 0) {
        size_t curlnum = heap->lnum[curtree];                 // current leonardo number
        char     *prev = root - LEONARDO[curlnum]*heap->size; // root of previous tree

        // find children of current root, trees < 2 don't have children
        char* lchild   = (curlnum >= 2) ? root - (LEONARDO[curlnum-2]+1)*heap->size : root;
        char* rchild   = (curlnum >= 2) ? root -                         heap->size : root;
        
        // only swap if previous root is larger than our root and our two chilren, if not,
        // the root is in the right place so we can break and bubble up the tree
        if (heap->cmp(prev, root)   < 1 ||
            heap->cmp(prev, lchild) < 1 ||                        
            heap->cmp(prev, rchild) < 1) break;

        // swap tree roots
        leo_swap(prev, root, heap->size);
        
        // move to previous tree and continue swapping
        curtree -= 1;
        root     = prev; 
    }

    // need to bubble up tree that we inserted into to restore heap property
    // compare root to both children, if it's larger than just one, swap those
    // two.  If it's larger than both, swap it with the larger of the two
    size_t curlnum = heap->lnum[curtree];
    while (curlnum > 1) {
        char *lchild = root - (LEONARDO[curlnum-2]+1)*heap->size;
        char *rchild = root -                         heap->size;
                
        uint8_t lcmp = heap->cmp(root, lchild) < 0; 
        uint8_t rcmp = heap->cmp(root, rchild) < 0;

        if (lcmp || rcmp) { 
            uint8_t joint = lcmp << 1 | rcmp;
            uint8_t lgtr  = heap->cmp(lchild, rchild) >= 1; 

            if (joint == 2 || (joint == 3 && lgtr)) { 
                leo_swap(root, lchild, heap->size);
                root     = lchild;
                curlnum -= 1;
            } else if (joint == 1 || (joint == 3 && !lgtr)) {
                leo_swap(root, rchild, heap->size);
                root     = rchild;
                curlnum -= 2;
            }
        } else {
            // neither child is greater, so quit
            break;
        }
    }
}


/*******************************************************************************
 * Process an element from the heap's data list into the heap.
 *
 * @param heap  pointer to leo_heap_t to modify
 * @param idx   element index in data array to add.
 *******************************************************************************/
static inline void leo_heap_insert_(leo_heap_t *heap, size_t idx) {
    heap->nmemb++;

    // if trees are of size L(n) and L(n-1) we can merge them to L(n+1) with new element
    if (heap->lnum[heap->ntree-1] == heap->lnum[heap->ntree-2]-1) {
        heap->ntree--;
        heap->lnum[heap->ntree-1]++;
    } else if (heap->lnum[heap->ntree-1] != 1) {
        heap->lnum[heap->ntree++] = 1;
    } else {
        heap->lnum[heap->ntree++] = 0;
    } 
    
    leo_heap_restore_(heap, heap->ntree-1, idx);
}


/*******************************************************************************
 * Remove an element from the heap.  This just leaves it at its proper location
 * in the data array.
 *
 * @param heap  heap_t instance to remove from
 *******************************************************************************/
static inline void leo_heap_remove_(leo_heap_t *heap) {
    heap->nmemb--;

    // for L(0) and L(1) trees, we can just remove as they contain a single element
    if (heap->lnum[heap->ntree-1] <= 1) {
        heap->ntree--;
    } else {
        // split last tree into two of L(n-1) and L(n-2)
        size_t lnum = heap->lnum[heap->ntree-1];
        heap->lnum[heap->ntree-1] = lnum-1;
        heap->ntree++;
        heap->lnum[heap->ntree-1] = lnum-2;

        // have to fix up left child and then right child
        leo_heap_restore_(heap, heap->ntree-2, heap->nmemb-(LEONARDO[lnum-2]+1));
        leo_heap_restore_(heap, heap->ntree-1, heap->nmemb-1);
    }
}


/*******************************************************************************
 * Sort an array using the smoothsort algorithm.  This builds a leonardo heap
 * in-place in the data and then deques to leave the data sorted.  The use of
 * a leonardo heap allows us to vary between O(n) and O(n*lg(n)) complexity
 * depending on how sorted the array is.
 *
 * @param dptr   pointer to data to sort
 * @param nmemb  number of elements in data array
 * @param size   size of element in bytes
 * @param cmp    comparison function to compare elements
 *******************************************************************************/
static inline void smoothsort(void *dptr, size_t nmemb, size_t size, cmp_func_t cmp) {
    if (nmemb <  2) return;  // already sorted

    // so we can do pointer arithmetic
    char *data = (char*)dptr;

    // initialize to starting conditions, no trees in the heap yet.
    leo_heap_t heap;
    heap.ntree = 0;
    heap.data  = data;
    heap.nmemb = 2;
    heap.size  = size;
    heap.cmp   = cmp;

    // first two trees are always size L(1) and L(0)
    heap.ntree   = 2;
    heap.lnum[0] = 1;
    heap.lnum[1] = 0;
    leo_cmp_and_swap(data, data + size, size, cmp);
    
    // build leonardo heap and then deque elements to leave data sorted
    for (size_t ii=2; ii < nmemb; ii++) leo_heap_insert_(&heap, ii);
    while (heap.nmemb > 0)              leo_heap_remove_(&heap);    
}



#endif//SMOOTHSORT_
