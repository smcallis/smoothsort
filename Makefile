override CC     := g++
override CCOPTS := -O3 -ansi -Wall -Wextra -I./ $(CCOPTS)

all: smoothtest

smoothtest: smoothtest.cc smoothsort.h
	$(CC) $(CCOPTS) $< -o $@

clean: 
	rm -f smoothtest

remake: clean all
